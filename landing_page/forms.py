from django import forms

from .models import PostModel

class PostForm(forms.ModelForm):
  class Meta:
    model = PostModel
    fields = [
      'comment',
    ]

    widgets = {
      'namaKegiatan' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'isi dengan keluh kesah kamu'
                }
            ),      
    }
