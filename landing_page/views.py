from django.shortcuts import render,redirect
from .forms import PostForm
from .models import PostModel

# Create your views here.

def index(request):
  post_form = PostForm(request.POST or None)
  post = PostModel.objects.all()

  if request.method == 'POST' :
    if post_form.is_valid():
      post_form.save()
  
  context = {
    'posts' : post,
    'post_form' : post_form,
  }

  return render(request, 'pages/index.html',context)
