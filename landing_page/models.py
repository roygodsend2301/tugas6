from django.db import models

# Create your models here.

class PostModel(models.Model):
    comment = models.CharField(max_length = 255)
    date = models.DateTimeField( auto_now_add = True)

    def __str__(self):
        return "{}".format(self.comment)
