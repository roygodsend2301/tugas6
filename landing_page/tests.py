from django.test import TestCase


# UNIT TEST
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
import unittest
from django.conf import settings
from importlib import import_module
from .models import PostModel as Comment
from django.utils import timezone
from .forms import PostForm as CommentForm



#FUNCTIONAL TEST
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys 
import time

# URL TEST
class UnitTest(TestCase):
  def test_landing_page_exist(self):
      response = Client().get('/')
      self.assertEqual(response.status_code , 200)

  def test_url_not_found(self):
      response = Client().get('/test')
      self.assertEqual(response.status_code, 404)

  # FUNCTION TEST
  def test_landing_page_using_index_function(self):
      found = resolve("/")
      self.assertEqual(found.func , index)

  # TEMPLATE USE TEST
  def test_landing_page_use_navbar(self):
      response = Client().get('/')
      self.assertTemplateUsed(response, 'pages/snipsets/navbar.html')
  
  def test_landing_page_use_javaScript(self):
      response = Client().get('/')
      self.assertTemplateUsed(response, 'pages/snipsets/javascript.html')

  #TEXT TEST
  def test_landing_page_content_has_content(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Halo!", html_response)
  
  def test_landing_page_content_has_content1(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Apa kabar kamu hari ini?", html_response)

  # MODEL TEST
  def test_model_create_new_comment(self):
    Comment.objects.create(
      comment = "Bebas banget hari ini",
      date = timezone.now()
    )
    count_all_comment = Comment.objects.all().count()
    self.assertEqual(count_all_comment,1)

  def test_comment_creation(self):
    obj = Comment.objects.create(
      comment = 'PPW mudah sekali',
      date = timezone.now()
    )

    self.assertTrue(isinstance(obj, Comment))
    self.assertEqual(obj.__str__(), obj.comment)
  #FORM TEST
  def test_valid_form(self):
    obj = Comment.objects.create(
      comment = 'Tidak ada yang sulit di dunia ini',
      date = timezone.now()
    )

    data = {
      'comment' : obj.comment,
      'date' : obj.date,
    }
    form = CommentForm(data=data)
    self.assertTrue(form.is_valid())

  def test_invalid_form(self):
    obj = Comment.objects.create(
      comment = "",
      date = 12
    )
    data = {
      'comment' : obj.comment,
      'date' : obj.date,
    }
    form = CommentForm(data=data)
    self.assertFalse(form.is_valid())

class CommentFunctionalTest(TestCase):
  def setUp(self):
    chrome_options = Options()
    chrome_options.add_argument('--dns-prefetch-disable')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('disable-gpu')

    self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
    super(CommentFunctionalTest,self).setUp()
  
  def tearDown(self):
    self.selenium.quit()
    super(CommentFunctionalTest,self).tearDown()

  def test_input_comment(self):
    selenium = self.selenium
    #open link
    selenium.get('http://127.0.0.1:8000/')
    #find element
    comment = selenium.find_element_by_name('comment')
    submit = selenium.find_element_by_class_name('tombol')
    #Input
    comment.send_keys("semangat!")
    time.sleep(3)
    submit.send_keys(Keys.RETURN)

